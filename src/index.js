import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import Board from './Board'
import * as serviceWorker from './serviceWorker'

ReactDOM.render(<Board count={50} />, document.getElementById('root'))

// If you want your Board to work offline and load faster, you can change
// unregister() to register() below. Board this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister()
